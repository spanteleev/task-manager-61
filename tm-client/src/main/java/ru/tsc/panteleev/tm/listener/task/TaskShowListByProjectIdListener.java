package ru.tsc.panteleev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.task.TaskGetListByProjectIdRequest;
import ru.tsc.panteleev.tm.dto.model.TaskDto;
import ru.tsc.panteleev.tm.event.ConsoleEvent;
import ru.tsc.panteleev.tm.util.TerminalUtil;

import java.util.List;

@Component
public class TaskShowListByProjectIdListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-list-show-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskShowListByProjectIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final List<TaskDto> tasks = getTaskEndpoint().showListByProjectIdTask(
                new TaskGetListByProjectIdRequest(getToken(), projectId)).getTasks();
        renderTasks(tasks);
    }

}
